#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

void err(int);
int main() {
	int intIN = 0;
	char goAgain = 'y';
	char inner = '*', outer = ' ';

	while (goAgain=='y' || goAgain=='Y') {
		int Q1, Q2, Q3, Q4;

		cout << "\n\n\t\t#########################\n"
		     << "\t\t### Diamond Generator ###\n"
		     << "\t\t#########################\n\n";
		cout << "\n\tEnter side length for diamond (1 to 40): ";
		cin >> intIN;

		Q3 = Q1 = intIN;
		cout << endl;
		while (cin.fail() || intIN < 1 || intIN > 40) { //sift bad input
			err(intIN);
			cout << "\n\tEnter side length for diamond (1 to 40): ";
			cin >> intIN;
			Q3 = intIN, Q1 = intIN;
			cout << endl;
		}

		for(int rowTop=0 ; rowTop < intIN ; rowTop++) {		// Top Half
			Q2 = rowTop;
			cout << "  "<< setfill(outer) << setw(intIN) << string (Q2, inner);
			Q2++;			
			
			cout << setfill(inner) << setw(intIN+1) << string (Q1, outer) << endl;		//Flip Q2 over x axis
			Q1-- ;	
		}
		
		for(int rowBtm=0 ; rowBtm <= intIN ; rowBtm++) {		// Bottom Half
			cout << "  "<< setfill(outer) << setw(intIN) << string (Q3, inner);	//Flip Q2 over y axis
			Q3--;

			Q4=rowBtm;
			cout << setfill(inner) << setw(intIN+1) << string (Q4, outer) <<endl ;	//Flip Q3 over x axis
			Q4++;
		}
		cout << "\n\t\tType y or Y to go again: ";
		cin >> goAgain;

		if (goAgain !='y' && goAgain !='Y') {
			cout << "\n\t\t\tGoodbye!\n\n\t\t";
			system("pause");
		}
	}

}
void err(int nr) { //fix letter, <1, >40
	if (cin.fail())
		cout << "\n\t\t/!\\ Not a number /!\\\n";
	else if (nr < 1)
		cout << "\n\t\t/!\\ Less than 1 /!\\\n";
	else if (nr > 40)
		cout << "\n\t\t/!\\ Greater than 40 /!\\\n";
	cin.clear();
	fflush(stdin);
}

